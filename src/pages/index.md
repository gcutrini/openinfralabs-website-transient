---
templateKey: index-page
seo:
  title: Home
  description: OpenInfra Labs
  url: https://openinfralabs.org/
  image: /img/svg/logo.svg
header:
  title: OPENINFRA LABS
  subTitle: <span>An <a href="https://osf.dev" target="_blank" rel="noopener">OSF</a> Project</span>
intro:
  title: Connecting open source projects to production    
  text: >    
    OpenInfra Labs is a community, created by and for operators, testing open source code in 
    production, publishing complete, reproducible stacks for existing & emerging workloads, 
    to advance open source infrastructure.    
features:
  title: Delivering open source tools to run cloud, container, AI, machine learning and edge workloads repeatedly and predictably
  featureList:
    - title: Integrated testing
      image: /img/svg/test.svg
      details: Integrated testing of all the necessary to provide a complete use case
    - title: Documentation
      details: Documentation of operational and funcional gaps required to run upstream projects in a production environment
      image: /img/svg/docs.svg
    - title: Share repositories
      details: Shared code repositories for operational tooling and the "glue" code that is often written indenpently by users
      image: /img/svg/code.svg
sponsors:
  title: Participants & Supporters
  sponsorList:
    - logo: /img/sponsors/boston-university.png
    - logo: /img/sponsors/harvard-university.png
    - logo: /img/sponsors/mit.png
    - logo: /img/sponsors/northeastern-university.png
    - logo: /img/sponsors/umass.png
    - logo: /img/sponsors/commonwealth-massachusetts.png
    - logo: /img/sponsors/intel.png
    - logo: /img/sponsors/red-hat.png
    - logo: /img/sponsors/futurewei.png
---

### Get Involved
If you are building or operating infrastructure for university or research usage, join forces with OpenInfra Labs today.

If you are a technology vendor or provider who would like to participate in OpenInfra labs, contact us at [info@openinfralabs.org](mailto:info@openinfralabs.org)

### Connect with the Community via these channels

- Freenode IRC: #openinfralabs
- Mailing list: <http://lists.opendev.org/cgi-bin/mailman/listinfo/openinfralabs>
